class Task < ActiveRecord::Base
  belongs_to :project
  validates :project_id, presence: true
  validates :name, presence: true, length: {maximum: 50}
  validates :description, presence: true, length: {maximum: 150}
  validates :progress_state, presence: true 
  
  enum progress_state: [:planned, :active, :complete]
end
