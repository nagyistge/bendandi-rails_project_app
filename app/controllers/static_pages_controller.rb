class StaticPagesController < ApplicationController
  def home
    @user = current_user
    if @user 
      @projects = @user.projects.all
    end
  end

  def about
  end
end
