class ProjectsController < ApplicationController
  #add access controls
  
  def new 
    @project = current_user.projects.build
  end
  
  def create
    @project = current_user.projects.build(project_params)
    if @project.save
    flash[:success] = "Project created!"
     redirect_to root_url #temp. change to project show for task adding  
    else
      render 'new'
    end
  end
  
  def show
    @project = Project.find(params[:id])
    @task = @project.tasks.build
    @tasks = @project.tasks.all
  end
  
  def edit
    @project = Project.find(params[:id])
  end
  
  def update
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      flash[:success] = "Project updated"
      redirect_to root_url
    else
      render 'edit'
    end
  end  
  
  def destroy
    Project.find(params[:id]).destroy
    flash[:success] = "project deleted"
    redirect_to root_url
  end
  
  private 
  
    def project_params
      params.require(:project).permit(:name, :description)
    end
end
