class AddProgressStateColumnToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :progress_state, :integer, default: 0
  end
end
