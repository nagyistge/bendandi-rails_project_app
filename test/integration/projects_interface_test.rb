require 'test_helper'

class ProjectsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:rick)
    log_in_as(@user)
  end
    
  test "invalid submission" do
    assert_no_difference 'Project.count' do
      post projects_path, project: {name: "", description: ""}
    end
    assert_select 'div#error_explanation'
  end
  
  test "valid submission" do
    assert_difference 'Project.count' do
      post projects_path, project: {name: "picnic", description: "fun for all the family"}
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match 'picnic',  response.body 
  end
end
