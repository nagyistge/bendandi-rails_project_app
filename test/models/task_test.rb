require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  
  def setup 
    @user = users(:rick)
    @project = @user.projects.build(name: "picnic", description: "good eats")
    @task = @project.tasks.build(name: "make tato salad", description: "eats pt.1")
  end
  
  test "should be valid" do
    assert_not @task.valid?
  end
  
  test "project id should be present" do
    @task.project_id = nil
    assert_not @task.valid?
  end
  
  test "name should be present" do
    @task.name = "   "
    assert_not @task.valid?
  end
  
  test "name should not be too long" do
    @task.name = "a" * 51
    assert_not @task.valid?
  end
  
  test "description should be present" do
    @task.description = " "
    assert_not @task.valid?
  end
  
  test "description should not be too long" do
    @task.description = "a" * 151
    assert_not @task.valid?
  end
  
  test "progress state should be present" do
    @task.progress_state = nil
    assert_not @task.valid? 
  end
  
end
