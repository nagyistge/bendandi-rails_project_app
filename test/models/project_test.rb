require 'test_helper'

class ProjectTest < ActiveSupport::TestCase

  def setup
    @user = users(:rick)
    @project = @user.projects.build(name: "Picnic", 
                           description: "We gonna have a picnic",
                           user_id: @user.id)
  end
  
  test "should be valid" do
    assert @project.valid?
  end
  
  test "user_id should be present" do
    @project.user_id = nil
    assert_not @project.valid?
  end
  
  test "name should be present" do
    @project.name = nil
    assert_not @project.valid?
  end
  
  test "name should not be too long" do
    @project.name = "a" * 51
    assert_not @project.valid?
  end
  
  test "description should be present" do
    @project.name = nil
    assert_not @project.valid?
  end
  
  test "description should not be too long" do
    @project.description = "a" * 251
    assert_not @project.valid?
  end
  
  test "project tasks are deleted with project delete" do
    @project.save
    @project.tasks.create(name:'sandwich', description: "tuna, wet")
    assert_difference 'Task.count', -1 do
      @user.destroy
    end
  end   
end
